# RNGeesus -- Misc -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/misc/rngeesus)

## Chal Info

Desc: `RNGeesus has a secret technique, can you guess it?`

Flag: `TUCTF{D0NT_1NS3CUR3LY_S33D_Y0UR_LCGS}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/rngeesus)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/rngeesus:tuctf2019
```
