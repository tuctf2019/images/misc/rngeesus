#!/usr/bin/env python2

# Linear congruential generator demo,
# inspiration taken with <3 from our crypto baron Dan Crowley
#import stuffs
from time import sleep
import sys

# The bare minimum LCG found at https://opensource.apple.com/source/Libc/Libc-1044.1.2/stdlib/FreeBSD/rand.c
def lcg(state):
    next = (state * 16807) % 2147483647
    return next


if __name__ == '__main__':
    #"seed" our LCG with the default number. It'll be predictable, but its hard to emulate srand(time(NULL)) on the backend.
    state = 123459876;

    #print out 'hints' for the user.
    print("Good afternoon, my children...\n")
    sys.stdout.flush()
    sleep(1)
    print("I've overheard the prayers to RNGeesus, but he's got a terrible secret...\n")
    sys.stdout.flush()
    sleep(1.5)
    print("He's just using rand() calls on his new Mac!\n")
    sys.stdout.flush()
    sleep(1)
    print("If you can guess his next number, I'll even give you a flag.\n")
    sys.stdout.flush()
    sleep(1)
    print("You ready to guess his next move? Here's his current random number.\n")
    sys.stdout.flush()
    #print out a rand() call
    rng = lcg(state)
    sleep(1)
    print("RNGesus gives the following number: {0}\n".format(rng))
    sys.stdout.flush()
    sleep(2)
    guess = input("Easy! Alright, what's his next move?\n")
    nrng = lcg(rng)
    if guess == nrng:
        print("You got it! Turns out the base call for rand() is a super simple Linear Congruential Generator.\n")
        sys.stdout.flush()
        sleep(1)
        print("Anyways, here's your flag. TUCTF{D0NT_1NS3CUR3LY_S33D_Y0UR_LCGS}")
        sys.stdout.flush()
        sys.exit(0)
    else:
        print("Hmm, that wasn't right. Try some more searching. There's source code out there somewhere! Better luck next time...")
        sys.stdout.flush()
        sys.exit(0)
