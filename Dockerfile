
FROM python:2

WORKDIR /usr/src/app

RUN apt -y update
RUN apt -y install socat

COPY ./src/superbadMac.py chal.py

RUN chmod +x /usr/src/app/chal.py

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/chal.py"
